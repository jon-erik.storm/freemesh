# v1.2.0 - Huge speed gain and upgraded to latest OpenWRT v21 release
The latest firmware v1.2.0 has been improved to gain maximum ethernet (~1Gbps) and wifi speed(~300Mbps)
Follow the screenshot below to configure firewall to advantage of the speed gain:
**Network -> Firewall -> Routing/NAT Offloading -> check both Software flow offloading and Hardware flow offloading**
![Flow Offloading Eth Speed](images/Flow_Offloading_Eth_Speed.PNG)

Freemesh nodes will require reflash and resetup


# FreeMesh router default
WiFi defaults - SSID: FreeMesh / FreeMesh2GHz , Passphrase: 9876543210
Connect to the router at 192.168.1.1
**Note: As of v1.2.0 any changes to these settings will need to be made prior to node setup. Changing these settings after node setup will require nodes to be factory reset and initialize again. We are currently working on a more secure way to update these setting on the nodes.**

# Configuring the router

You should set up several things before configuring the mesh nodes:
1. Plug in the router
1. Connect your laptop or PC to the FreeMesh wireless access point using the passphrase above. 
1. Open your browser to http://192.168.1.1 -- this opens the LuCI interface.
1. Set up a root password for LuCi by following the instructions
1. Click on the Network pulldown and select Wireless.
1. You'll see two SSIDs, one for 2GHz and one for 5GHz. Click on Edit for each of these and set the SSID
1. click on the Wireless Security tab and change the Key. This will be your new wireless password.
1. You can use the same SSID for both radios if you want. Your devices will choose one (usually the best one) when they connect. If you do this, the passwords must be the same!
1. Save your changes, and then make sure to apply them
1. Reconnect to your new network SSID and make sure it works before setting up the mesh devices.

# FreeMesh mesh setup instructions

1. Connect the node (blue WAN port) to the router (yellow LAN port). Note - Both wifi lights should be alternating to indicate the node is in initialize mode.\
![Step 1 Image](images/Step1.png)
1. Once node’s wifi lights are flashing in sync, The node can be unplugged and moved.\
![Step 2 Image](images/Step2.png)
1. Once the node has been moved and powered on, You should see both wifi lights turn on solid. This means the node has a connection to the router. Note: It can take a few minutes for the wifi lights to come on after boot. If wifi lights do not come on after a few minutes, the node will probably need to be moved to a different location.
1. Repeat steps 1 through steps 3 for each additional node.

Click the image below for the YouTube video

[![YouTube video](images/thumbnail.png)](http://www.youtube.com/watch?v=7tO_3ty7gps)

# Access via SSH
**Note: This is for advanced users**

The router has ssh enabled, so you can log in as root there.
From a connected machine, just run `ssh router`. The mesh nodes
get assigned IP addresses starting from 1 higher than the router.
For example, by default, the first mesh node will e 192.168.1.2.
To log in to these nodes, first ssh to the router, then ssh to
the mesh node. The router has a certificate so no password will
be required.

## Updating nodes
To update the software packages on the nodes, you have to use the
ssh command line since there isn't a UI. To do that, run these commands:

    opkg update
    opkg list-upgradable | awk '{print $1}' | xargs opkg upgrade

## Latest builds
You can download the latest builds at the links below.
Router: https://gitlab.com/slthomason/freemesh/-/blob/master/builds_by_version/fm.v1.2.0.router.we1326.bin
Node: https://gitlab.com/slthomason/freemesh/-/blob/master/builds_by_version/fm.v1.2.0.node.we826.bin

## To Compile
**Note: Before compiling install 'make' utility and development tools. You can find resources at https://openwrt.org/docs/guide-developer/build-system/install-buildsystem.**
1. Clone repo - git clone https://gitlab.com/slthomason/freemesh.git
1. cd /freemesh
1. Make a directory for the firmware - mkdir \<path to store builds\>
1. Compile router - ./compile.sh zbt-we1326
1. Copy bin - cp bin/targets/ramips/mt7621/openwrt-ramips-mt7621-zbtlink_zbt-we1326-squashfs-sysupgrade.bin \<path to directory from step 3\>/we1326.router.bin
1. Compile node - ./compile.sh zbt-we826-16m
1. Copy bin - cp bin/targets/ramips/mt7620/openwrt-ramips-mt7620-zbtlink_zbt-we826-16M-squashfs-sysupgrade.bin \<path to directory from step 3\>/we826.node.bin

## Flashing The Router and Nodes
1. With the power off, use a pen to hold down the reset button.
2. While continuing to hold the reset button, power on the unit.
3. Hold the reset button until you see the 2GHz light blinking rapidly. This could take up to 30 seconds. Don't proceed until you see this rapid blinking.
4. Connect the one of the unit's LAN ports (yellow) to a laptop or PC.
5. Configure your laptop or PC to use a static address 192.168.1.2, netmask 255.255.255.0
6. Enter 192.168.1.1 in a browser.
7. Depending on your hardware version, you'll either have a simple UI to upload a .bin file or a complex menu. It might be in Chinese.  If it's a simple menu, just click the button to upload the correct .bin file.

Instead, if there's a complex UI, follow these steps:
**Step 1:** Click the button shown below.
![Step 1 UI Image](images/new_ui1.PNG)
**Step 2:** Select the second option in the menu to the left.
**Step 3:** Upload the bin file by clicking the button shown below.
**Step 4:** Click the button at the bottom.
![Step 2,3,4 UI Image](images/new_ui2.PNG)
**Step 5:** Click button shown below to confirm.
![Step 5 UI Image](images/new_ui3.PNG)
8. Wait until you see the 2nd and 3rd LEDs blinking together. This will tell you that the firmware has flashed successfully.

## Changing Settings
Currently you can change SSIDs and keys that will sync to the nodes. Changing IPs, channel, etc will require you to reset node and re-connect to the router.
Feel free to extend this functionality at https://gitlab.com/slthomason/freemesh/blob/master/package/freemesh/cfg-router/files/freemesh/www/handler.cgi and submit a pull request.
**Note: Changing the 5GHz channel will break the backchannel for the mesh.**

# 4G LTE Configuration
**4G services not provided. You will still need to activate with your carrier.**
Edit the /etc/config/network config Make sure the entry below is in the config.

config interface wan
\# option ifname ppp0 \# on some carriers enable this line
option pincode 1234
option device /dev/ttyUSB0
option apn your.apn
option service umts
option proto 3g

## T-Mobile
The /etc/config/network should look similar to this:
config interface 'wan_ppp'
option device '/dev/ttyUSB2'
option metric '20'
option apn 'fast.t-mobile.com'
option service 'umts'
option proto '3g'

**Note: If your sim card doesn't have a pincode you can omit that in the config**

## Ethernet Backhaul
FreeMesh nodes can be connected to the router via an Ethernet cable creating a wired backhaul. Ethernet backhaul also works with managed switches that have STP support.
Connect the node(s) to the router using a LAN to LAN connection.
STP is enabled to prevent loops in network bridge.
When Ethernet is disconnected the connection will failover to wireless.
**Note: It could take up to 1 minute to connect to wireless after disconnecting from Ethernet.**
![Backhaul Diagram](images/backhaul.png)

# VPN setup instructions (v1.2.0)  

**Note: This release still contains an open issue regarding routing.**  

1. Connect to the OpenWrt2 network.
1. Open a browser and visit 192.168.1.1.
1. Log in to the admin page with username and password. Note: Password will be blank on the first login. 
1. Navigate to VPN -> LanBridge-Cient. On this page, you can download, delete, and re-generate configs.
1. Download config.
**Note: Download the TAP config for desktop and TUN config for mobile**

## Windows Client Setup
1. Download OpenVPN Cient at https://openvpn.net/community-downloads/. 
1. Follow the wizard to install the client. 
1. Launch OpenVPN GUI.
1. Select import file and find the config you downloaded.
1. Click connect to connect to your VPN.

## macOS Client Setup
1. Download tunnelblick at https://tunnelblick.net/cInstall.html.
1. Folow the wizard to install the client.
1. Drag and drop the config you downloaded in the tunneblick icon located in the top-right toolbar.
1. Left click on the tunnelblick icon and select connect.

## Linux Client Setup
1. Download OpenVPN Cient at https://openvpn.net/community-downloads/. 
1. Open a terminal and run the command sudo openvpn --config ./\<location of downloaded config\>/\<name of config\>.ovpn

## iOS/Android Client Setup
1. Download OpenVPN Connect in the Apple App Store or Google Play Store.
1. Open the app and import the TUN config you downloaded in the VPN setup.
1. Once successfully imported you tap the connect button in the list of profiles.

## Useful Resources
login - https://openwrt.org/docs/guide-quick-start/walkthrough_login
wifi - https://openwrt.org/docs/guide-quick-start/walkthrough_wifi
ssh - https://openwrt.org/docs/guide-quick-start/sshadministration
packages - https://openwrt.org/packages/start
troubleshooting - https://openwrt.org/docs/guide-quick-start/checks_and_troubleshooting
uci - https://openwrt.org/docs/guide-user/base-system/uci
luci - https://openwrt.org/docs/guide-user/luci/start
