#!/bin/sh

if [ ! -e "/etc/easy-rsa/pki/private/ovpnserv.key" ];then
	#generate encryption keys for server
	/bin/ovpn-gen-server ovpnserv &
fi

#Check if it already in config then skip.
uci get openvpn.lanvpntap.enabled
if [ "$?" == "1" ];then
	#Configure openvpn server parameters
	uci rename openvpn.$(uci add openvpn openvpn)=lanvpntap
	uci set openvpn.lanvpntap.enabled=1
	uci set openvpn.lanvpntap.port='1195'
	uci set openvpn.lanvpntap.proto='udp'
	uci set openvpn.lanvpntap.dev='tap0'
	uci set openvpn.lanvpntap.keepalive='5 15'
	uci set openvpn.lanvpntap.persist_key='1'
	uci set openvpn.lanvpntap.persist_tun='1'
	uci set openvpn.lanvpntap.client_to_client='1'
	uci set openvpn.lanvpntap.tls_server='1'
	uci set openvpn.lanvpntap.ifconfig_pool_persist='/tmp/ipp-tap.txt'
	uci set openvpn.lanvpntap.ca='/etc/easy-rsa/pki/ca.crt'
	uci set openvpn.lanvpntap.cert='/etc/easy-rsa/pki/issued/ovpnserv.crt'
	uci set openvpn.lanvpntap.key='/etc/easy-rsa/pki/private/ovpnserv.key'
	uci set openvpn.lanvpntap.dh='/etc/openvpn/keys/ovpnserv.pem'
	uci set openvpn.lanvpntun.duplicate_cn='1'

	#set proper IP range to push to client. Works only with 255.255.255.0 (/24) netmask.
	NET="$(uci get network.lan.ipaddr|awk -F. '{printf("%d.%d.%d\n", $1,$2,$3)}')"
	uci set openvpn.lanvpntap.server_bridge="$(uci get network.lan.ipaddr) $(uci get network.lan.netmask) $NET.10 $NET.30"

	#Add bogus interface description so tap0 will be always available
	uci set network.lanvpntap=interface
	uci set network.lanvpntap.ifname=tap0
	uci set network.lanvpntap.proto=none
	uci set network.lanvpntap.auto=1
fi

#Check if it already in config then skip.
uci get openvpn.lanvpntun.enabled
if [ "$?" == "1" ];then
	#Configure openvpn server parameters
	uci rename openvpn.$(uci add openvpn openvpn)=lanvpntun
	uci set openvpn.lanvpntun.enabled=1
	uci set openvpn.lanvpntun.port='1196'
	uci set openvpn.lanvpntun.proto='udp'
	uci set openvpn.lanvpntun.dev='tun0'
	uci set openvpn.lanvpntun.keepalive='5 15'
	uci set openvpn.lanvpntun.tun_mtu='1500' 
	uci set openvpn.lanvpntun.persist_key='1'
	uci set openvpn.lanvpntun.persist_tun='1'
	uci set openvpn.lanvpntun.client_to_client='1'
	uci set openvpn.lanvpntun.tls_server='1'
	#uci set openvpn.lanvpntun.ifconfig_pool_persist='/tmp/ipp-tun.txt'
	uci set openvpn.lanvpntun.ca='/etc/easy-rsa/pki/ca.crt'
	uci set openvpn.lanvpntun.cert='/etc/easy-rsa/pki/issued/ovpnserv.crt'
	uci set openvpn.lanvpntun.key='/etc/easy-rsa/pki/private/ovpnserv.key'
	uci set openvpn.lanvpntun.dh='/etc/openvpn/keys/ovpnserv.pem'

	#set proper IP range to push to client. Works only with 255.255.255.0 (/24) netmask.
	# we're using 28 prefixlen (255.255.255.240) to allocate VPN addresses.
	#NET="$(uci get network.lan.ipaddr|awk -F. '{printf("%d.%d.%d\n", $1,$2,$3)}')"
	#Assign different subnet mask other than primary lan such as 192.168.1.1
	uci set openvpn.lanvpntun.server="192.168.200.0 255.255.255.0"

	uci set openvpn.lanvpntun.mode='server'
	uci set openvpn.lanvpntun.topology='subnet'
	uci set openvpn.lanvpntun.duplicate_cn='1'
	#uci add_list openvpn.lanvpntun.push='redirect-gateway def1'
	#Route from router network
	uci add_list openvpn.lanvpntun.push="route 192.168.1.0 255.255.255.0"

	#Push all clients on lan to access remotely
    uci add_list openvpn.lanvpntun.push="dhcp-option DNS 192.168.1.1"

	#Add bogus interface description so tap0 will be always available
	uci set network.lanvpntun=interface
	uci set network.lanvpntun.ifname=tun0
	uci set network.lanvpntun.proto=none
	uci set network.lanvpntun.auto=1
fi


if [ x"$(uci get network.lan.ifname|grep tap0)" == "x" ];then
	#Add interface to bridge
	uci set network.lan.ifname="$(uci get network.lan.ifname) tap0"
fi

#Add Firewall rules to Accept incoming connection on WAN
uci set firewall.OpenVPNtap_In=rule
uci set firewall.OpenVPNtap_In.target=ACCEPT
uci set firewall.OpenVPNtap_In.src=*
uci set firewall.OpenVPNtap_In.proto=udp
uci set firewall.OpenVPNtap_In.dest_port=1195
uci set firewall.OpenVPNtun_In=rule
uci set firewall.OpenVPNtun_In.target=ACCEPT
uci set firewall.OpenVPNtun_In.src=*
uci set firewall.OpenVPNtun_In.proto=udp
uci set firewall.OpenVPNtun_In.dest_port=1196

#Add Firewall rule for proxy_arp to TUN interface
uci set firewall.@zone[0].network='lan lanvpntun lanvpntap'

#On Luci Generation we need to wait further for client config generation to complete.
uci set uhttpd.main.script_timeout=240

/etc/init.d/openvpn enable

sleep 1

exit 0;

