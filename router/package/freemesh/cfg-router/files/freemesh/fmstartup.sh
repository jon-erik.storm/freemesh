#!/bin/sh

#This script will everytime on boot
#We can do various tasks here

#Check if vpn certs are generated or not, and do accordingly

#Vpn server
if [ ! -e "/etc/easy-rsa/pki/private/ovpnserv.key" ];then
	#generate encryption keys for server
	/bin/ovpn-gen-server ovpnserv &

    #Regenerate clients config everytime after ssl key regenerates
    sleep 1;
	/bin/ovpn-gen-client ovpn-client auto tap;
	/bin/ovpn-gen-client ovpn-client auto tun;

fi

#Vpn Client
if [ ! -e "/etc/openvpn/clients/ovpnclient.ovpn" ];then
	sleep 1;
	/bin/ovpn-gen-client ovpn-client auto tap;
	/bin/ovpn-gen-client ovpn-client auto tun;
fi

#Below command to unlock locked mount
mtd unlock rootfs_data
mount -o remount,rw /