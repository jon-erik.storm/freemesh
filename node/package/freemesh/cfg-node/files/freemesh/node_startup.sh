#!/bin/sh
#run on startup each time

random_key() {
	tr -dc A-NP-Za-km-z2-9 </dev/urandom | head -c ${1:-16}
}

first_boot() {
	echo "`date` - first_boot() start" >> /tmp/fm.log;

	uci set system.@system[0].hostname="mesh-node-$(random_key 4)";
	uci set fm.node.state=initialize;
	uci commit;

	/freemesh/flashled.sh 0 &

	echo "`date` - first_boot() end" >> /tmp/fm.log;
}

# gets the default route address
gateway() {
	ip ro | awk '$1 == "default" {print $3}'
}

# gets the ethernet port mac
mac() {
	ifconfig eth0 | awk 'NR == 1 {print $5}' | tr A-F a-f
}

remote_log() {
	gw=$1
	message=$2
        wget -q -O - http://$gw/cgi-bin/handler.cgi?l="$message"
}

leds() {
	echo $1 > /sys/class/leds/mt76-phy0/brightness;
	echo $2 > /sys/class/leds/zbt-we826\:green\:wifi/brightness;
}


# startup LED sequence
# ON ON - waiting for gateway
#         stuck here means you aren't getting anything from the upstream
#         port. Did you plug it in via the BLUE connector on the node and
#         a YELLOW connector on the router?
#         This is also a normal state after doing a fresh reflash
# OFF ON - waiting for ping from gateway
#          stuck here means your network isn't responding correctly
# ON OFF - getting configuration
#          stuck here might mean there is a mismatch between firmware version
#          on the router and node, or you're not plugged in to a real router
# FLASH FLASH - all done

initialize() {
	# preserve stderr, then redirect it to /tmp/fm.log
	exec 3>&2
	exec 2>>/tmp/fm.log

	echo "$(date) - looking for a gateway" >&2
	leds 1 1
	gw=$(gateway)
	while [ -z "$gw" ]
	do
		sleep 1
		gw=$(gateway)
	done

	echo "$(date) - gateway $gw ping start" >&2
	leds 0 1
	# ping the gateway with the LEDs on steady
	while ! ping -c 1 $(gateway) 2>/dev/null
	do
		sleep 1
		gw=$(gateway)
	done

	gw=$(gateway)
	echo "$(date) - ping successful; gateway $gw" >&2
	leds 1 0
	set -x
	uci set fm.node.gateway_ip="$gw"
	uci set network.lan.gateway="$gw"
	uci add_list dhcp.@dnsmasq[0].server="$gw"

	uci set batmand.general.interface="bat0"

	# keep trying to get the configuration until it gets at least
	# the wireless.mesh_five.key value
        mac=$(mac)
	while :
	do
	    echo "$(date) - reading configuration from $gw" >&2
	    wget -q -O - http://$gw/cgi-bin/handler.cgi?mac=$mac |
		while read opt
		do
			# do some basic filtering; the server can
			# only set wireless parameters or the static
			# ip address
			case $opt in
				wireless.*=*)
					eval uci set $opt;;
				network.lan.ipaddr=*)
					eval uci set $opt;;
				fm.*=*)
					eval uci set $opt;;
			esac
		done
	    [ -z "$(uci get wireless.mesh_five.key)" ] || break
	done

	uci set dhcp.lan.ignore=1
	uci commit
	echo "$(date) - committed changes" >&2

	/freemesh/change_pw.sh &

	hostname=$(uci get system.@system[0].hostname)
	remote_log $gw \
		"config-success,hostname=$hostname,gw=$gw,$secure"

	# Tell router to increment next_static
	myip=$(uci get network.lan.ipaddr)
	wget -O - "http://$gw/cgi-bin/handler.cgi?accept=$mac,$hostname,$myip" >&2

	#flash leds on initialize end
	/freemesh/flashled.sh 0 &
	echo "$(date) - started flashing LEDs" >&2

	# done -- set state to normal
        uci set fm.node.state=normal
        uci commit fm

	echo "`date` - initialize() end" >&2
	# turn off logging
	set +x

	# restore stderr
	exec 2>&3
	exec 3>&-
}

manual_leds() {
	#kill the trigger so we control the leds
	echo "none" > /sys/class/leds/mt76-phy0/trigger;
	echo "none" > /sys/class/leds/zbt-we826\:green\:wifi/trigger;
}

node_connectivity_check()
{

    #create a flag to indicate router booted normally
	touch /tmp/normal_boot;

	#call this method after node state is stable
	#setup cron job ....
	crontab -l > /etc/crontabs/root;
	echo "0-59/1 * * * * /freemesh/check_router_ap.sh" >> /etc/crontabs/root;
	/etc/init.d/cron start;

}

# make the LEDs visible
echo 1 > '/sys/class/leds/mt76-phy0/brightness'
echo 1 > '/sys/class/leds/zbt-we826:green:wifi/brightness'

case "$(uci get fm.node.state)" in
	firstboot)
		manual_leds
		first_boot;;
	initialize)
		manual_leds
		initialize;;
	normal)	
		node_connectivity_check;;
esac
