#!/bin/sh

if [ x"$1" == "x" ];then
	echo "USAGE:";
	echo "   $0 <profile-name> [remote-addr] [conntype]";
	echo "Where";
	echo "    <profile-name>    is mandatory option, name of client keys";
	echo "    [remote-addr]     is optional parameter to use as remote connection (WAN ip).";
	echo "                      can be 'auto' to set from WAN interface or DNS name or IP.";
	echo "    [conntype]        is optional parameter to select type of connection.";
	echo "                      can be either 'tap' or 'tun'. 'tap' is default.";
	echo "                      'tap' is used for desktop clients, 'tun' for mobile.";
	echo "Examples:";
	echo "   $0 ovpnclient1 auto";
	echo "   $0 ovpnclient2 1.2.3.4 tun";
	echo "   $0 ovpnclient3 bogus.dyndns.org";
	echo;
	echo "Output configuration will be saved to /etc/openvpn/clients/<profile-name>-<conntype>.ovpn";
	exit 0;
fi

PROFILE="$1";
REMOTE="$2";
TYPE="$3";

if [ x"$REMOTE" == "x" ];then
	REMOTE="auto";
fi

if [ x"$TYPE" == "x" ];then
        TYPE="tap";
fi

mkdir -p /etc/openvpn/keys
mkdir -p /etc/openvpn/clients

if [ x"$REMOTE" == "xauto" ];then
	IFNAME="$(uci get network.wan.ifname)";
	TEMPADDR="$(ip addr show dev $IFNAME|grep inet |grep -v inet6|awk '{print $2}'|sed 's/\/.*//')";
	if [ x"$TEMPADDR" == "x" ];then
		echo "Cannot acquire WAN address for remote 'auto' parameter. Use static global IP or DNS-name instead of 'auto'.";
		exit 0;
	fi
	REMOTE="$TEMPADDR";
fi

if [ ! -s "/etc/easy-rsa/pki/issued/$PROFILE.crt" ];then
        easyrsa --pki-dir=/etc/easy-rsa/pki/ --req-c=US --req-st=WS --req-city=WS --req-org=CleanRouter --req-email=void@domain.tld --req-ou=CR --batch --days=3650 --req-cn=CR build-client-full $PROFILE nopass
fi

OVPNCONFIG="/etc/openvpn/clients/$PROFILE-$TYPE.ovpn";
if [ "$TYPE" == "tap" ];then
        cat <<EOC >$OVPNCONFIG
client
remote $REMOTE
dev tap
proto udp
port 1195
keepalive 10 120
persist-key
persist-tun
resolv-retry infinite
remote-cert-tls server
EOC

else
        cat <<EOC >$OVPNCONFIG
client
remote $REMOTE
dev tun
proto udp
port 1196
keepalive 10 120
persist-key
persist-tun
resolv-retry infinite
remote-cert-tls server
EOC

fi

echo "<ca>" >>$OVPNCONFIG;
cat /etc/easy-rsa/pki/ca.crt >>$OVPNCONFIG;
echo "</ca>" >>$OVPNCONFIG;
echo "<cert>" >>$OVPNCONFIG;
cat /etc/easy-rsa/pki/issued/$PROFILE.crt >>$OVPNCONFIG;
echo "</cert>" >>$OVPNCONFIG;
echo "<key>" >>$OVPNCONFIG;
cat /etc/easy-rsa/pki/private/$PROFILE.key >>$OVPNCONFIG;
echo "</key>" >>$OVPNCONFIG;





